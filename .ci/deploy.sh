#!/bin/sh

# create a public directory
mkdir public

# define the name of the upstream project
groupName="R3%2Foutreach%2Ftemplates"
projectName="paper"
nameFile="main.pdf"

# determine the upstream project ID
fullURL="$CI_API_V4_URL/projects/$groupName%2F$projectName"
echo $fullURL
UPSTREAM_CI_PROJECT_ID=$(curl --request GET --header "Private-Token: $BOT_TOKEN" $fullURL | jq '.id')

# If the PR_ID is empty, then one of the following is true:
#   - the merge request is not opened on the fork directly, but on the upstream repository.
#   - there is no merge request
echo " -------------------------------------------------------------------------------- "
PR_ID=$(git ls-remote $CI_REPOSITORY_URL | awk "/$CI_BUILD_REF/"'{print $2}' | awk /merge-requests/'{print $1}' | cut -d '/' -f3)

if [ ! -z "$PR_ID" ]; then # the MR is opened in the fork
    # set the download branch name to download the artifacts
    downloadBranch=$CI_COMMIT_REF_NAME

    echo " > The merge request is opened on the same repository."
    echo " > The merge request ID is: $PR_ID."
else # the MR is openend from the fork to the upstream repository

    # build the full URL
    fullURL="$CI_API_V4_URL/projects/$UPSTREAM_CI_PROJECT_ID/merge_requests"

    # retrieve the list of the opened pull requests in the upstream repository
    PR_ID=$(curl --request GET --data-urlencode "state=opened" --header "Private-Token: $BOT_TOKEN" $fullURL | jq '.[] | select(.source_branch=="'$CI_COMMIT_REF_NAME'") | .iid')

    # set the project ID
    CI_PROJECT_ID=$UPSTREAM_CI_PROJECT_ID

    # set the download branch name to download the artifacts
    downloadBranch="master"

    # print msg
    if [ ! -z "$PR_ID" ]; then
        echo " > The merge request is opened from a fork. Upstream project ID: $CI_PROJECT_ID."
        echo " > The merge request ID is: $PR_ID."
    else
        echo " > This build is not triggered by a merge request."
    fi
fi
echo " -------------------------------------------------------------------------------- "

# download the existing artifacts from the pages job on master
archiveURL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/artifacts/$downloadBranch/download?job=pages"
echo " > The archive URL is $archiveURL"
curl --header "PRIVATE-TOKEN: $BOT_TOKEN" --location --output artifacts.zip $archiveURL


# quietly unzip the artifacts
unzip -qq artifacts.zip
rm artifacts.zip

# rename the previous public folder as archive
if [ -d "public" ]; then
    mv public public-archive
fi

# create a new public folder for the artifacts
mkdir -p public
comment=false

# determine the commit message
commitMsg="$(git log -1 --pretty=%B | awk '{print tolower($0)}')"

# loop through all the branches
for branch in $(git for-each-ref --format='%(refname:strip=3)' refs/remotes); do

    # print the name of the branch
    echo " > The branch is $branch"

    if [ $branch == "master" ]; then
        artifact="stable"
    elif [ $branch == "develop" ]; then
        artifact="latest"
    else
        artifact="$branch"
        comment=true
        buildArtifact=true
    fi

    # determine if a build should happen for stable and latest
    # this only holds for merge requests.

    # use force to rebuild
    if [ $commitMsg == *"force"* ]; then
        buildArtifact=true
        echo " > All pages will be rebuilt as commit message includes 'force'"
    else
        if [ $commitMsg == *"request"* ]; then
            buildArtifact=true
            echo " > The commit is a merge commit. Latest and stable will be rebuilt."
        else
            if [ $branch == "master" ] || [ $branch == "develop" ]; then
                if [ -d "public-archive/$artifact" ]; then
                    buildArtifact=false
                    echo " > The $artifact version exists in the archive and will be used."
                else
                    buildArtifact=true
                    echo " > The $artifact version does not exist in the archive. A new $artifact version will be built."
                fi
            fi
        fi
    fi

    echo " > The artifact folder is $artifact";

    if [ "$buildArtifact" == true ] ; then
        # print msg
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "

        # checkout the respective branch
        git checkout -f $branch
        git reset --hard $branch

        # build artefacts
        # move the asset to public folder
        mkdir public/$artifact
        cp $nameFile public/$artifact/.

        # print msg
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "
    fi
done

# checkout the current branch
echo $CI_COMMIT_REF_NAME
git checkout -f $CI_COMMIT_REF_NAME
git reset --hard origin/$CI_COMMIT_REF_NAME

# reset the artifact name
artifact="$CI_COMMIT_REF_NAME"

# remove the potential old build in the archive folder
if [ -d "public-archive" ]; then
    rm -rf public-archive/$branch
    rm public-archive/index.html
fi

# copy the public-archive to public
if [ "$buildArtifact" == false ] ; then
    cp -r public-archive/* public/.
    echo " > The existing folders from the archive have been copied to the public folder"
fi

# remove the archive folder
if [ -d "public-archive" ]; then
    rm -rf public-archive
    echo " > Archive folder removed."
fi

# report the status
if [ "$comment" == true ] && [ ! -z "$PR_ID" ]; then

    note=":white_check_mark: The artefacts have been deployed successfully: [${CI_PAGES_URL}/$artifact/$nameFile](${CI_PAGES_URL}/$artifact/$nameFile)"
    fullURL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/merge_requests/$PR_ID/notes"
    echo $PR_ID
    echo $note
    echo $fullURL

    curl --request POST --data-urlencode "body=$note" --header "Private-Token: $BOT_TOKEN" $fullURL
fi

# end message
echo " > The artifacts have been uploaded."
