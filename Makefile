REFS=bib/paper-refs.bib
INCLUDES=$(wildcard src/*.tex)
DATADIR=data
DATA=$(wildcard data/*)
PLOTSDIR=plots
PLOTSCRIPTS=$(wildcard $(PLOTSDIR)/*.R)
PLOTS=$(PLOTSCRIPTS:.R=.png)
MEDIA=$(wildcard media/*)
MAIN=main.tex
OUT=$(MAIN:.tex=.pdf)
DOCKERURL=git-r3lab.uni.lu:4567

.PRECIOUS: $(PLOTS) $(REFS)

$(OUT): $(MAIN) $(INCLUDES) $(REFS) $(MEDIA) $(PLOTS)
	docker run -it --mount src=$(PWD),target=/paper,type=bind -w /paper $(DOCKERURL)/r3/docker/latex /bin/sh -c "pdflatex $<; bibtex $(<:.tex=); pdflatex $<; pdflatex $<"

$(PLOTSDIR)/%.png: $(PLOTSDIR)/%.R $(DATA)
	docker run -t -v $(PWD)/$(DATADIR):/$(DATADIR) -v $(PWD)/$(PLOTSDIR):/$(PLOTSDIR) $(DOCKERURL)/r3/docker/r-ggplot2 R --vanilla -f $<

$(REFS):
	docker run -t -v $(PWD)/bib/zotero.ini:/config/zotero.ini -v $(PWD)/bib:/bib $(DOCKERURL)/r3/apps/zotero

view:
	open $(OUT)

clean:
	bash -c 'rm -fv *.{log,dvi,aux,toc,lof,out,blg,bbl} $(OUT) $(REFS)'
	rm -fv $(PLOTS)
